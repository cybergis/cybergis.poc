from z3c.form import group, field
from zope import schema
from zope.interface import invariant, Invalid
from zope.schema.interfaces import IContextSourceBinder
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm


from plone.jsonapi.core import router
from plone.jsonapi.routes.api import url_for

# CRUD

from plone.jsonapi.routes.api import get_items, get_batched
from plone.jsonapi.routes.api import create_items
from plone.jsonapi.routes.api import update_items
from plone.jsonapi.routes.api import delete_items

from Products.CMFCore.utils import getToolByName
from plone import api

topic_keys = ['Title','author_name','id',
                'Description','ExpirationDate','created',
                'modified','review_state','Creator','Subject',
                'UID','effective']

data_keys = ['Title','UID','Subject','ExpirationDate','Description',
                'created','modified','review_state','id','Creator',
                'effective']

def populate_list(items, results, desired_keys=[]):
    for item in results:
        obj = item.getObject()
        fields = obj.to_dict()
        for k in item.__record_schema__.keys():
            if item[k] and k in desired_keys:
                fields[k] = str(item[k])
            elif k in desired_keys:
                fields[k] = []
        items.append(fields)
 
# GET
# /Plone/@@API/topics
@router.add_route("/topics","topics",methods=["GET", "POST"])
def get(context, request, uid=None) :
    '''Get all topics
       AKA all items of type cybergis.poc.topic
    '''
    global topic_keys
    items = []
    catalog = getToolByName(context, 'portal_catalog')
    results = catalog(portal_type = "cybergis.poc.topic",uid=uid)
    populate_list(items, results, topic_keys)
    return {
        "count": len(items),
        "items": items,
    }

# /Plone/@@API/topics/<topic[id]>
@router.add_route("/topics/<string:topic_id>","topic_id",methods=["GET"])
def get(context, request, topic_id) :
    global topic_keys
    global data_keys
    topic_path = ''
    items = []
    topic_keys = ['Title','author_name','id','start','end',
                    'Description','ExpirationDate','created',
                    'modified','review_state','Creator','Subject',
                    'UID','effective']
    catalog = getToolByName(context, 'portal_catalog')
    topic_result = catalog(portal_type = "cybergis.poc.topic", id=topic_id)
    if (len(topic_result) <= 0) :
        return {
            "count": 0,
            "items": None,
        }
    if topic_result[0]:
       topic_path = topic_result[0].getPath()
    topic_childs = catalog(path={'query':topic_path, 'depth':1}, portal_type="cybergis.poc.topic")
    data_childs = catalog(path={'query':topic_path, 'depth':1}, portal_type="cybergis.poc.data")
    populate_list(items, topic_result,topic_keys)
    populate_list(items, topic_childs,topic_keys)
    populate_list(items, data_childs,data_keys)
    print len(topic_childs)
    print len(data_childs)
    return {
        "count": len(items),
        "items": items,
    }

