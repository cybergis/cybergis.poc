from z3c.form import group, field
from zope import schema
from zope.interface import invariant, Invalid
from zope.schema.interfaces import IContextSourceBinder
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm

from plone.dexterity.content import Container

from plone.app.textfield import RichText
from plone.namedfile.field import NamedImage, NamedFile
from plone.namedfile.field import NamedBlobImage, NamedBlobFile
from plone.namedfile.interfaces import IImageScaleTraversable

from z3c.relationfield.schema import RelationList, RelationChoice
from plone.formwidget.contenttree import ObjPathSourceBinder

from plone.supermodel import model
from Products.Five import BrowserView

from cybergis.poc import MessageFactory as _
from cybergis.poc.vocabulary import *
from Acquisition import aq_inner
from Products.CMFCore.utils import getToolByName
import cybergis.poc.jsonapi

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.uuid.interfaces import IUUID
from plone.app.uuid.utils import uuidToObject

# Interface class; used to define content-type schema.

class IData(model.Schema, IImageScaleTraversable):
    """
    The metadata for a data source used in the POC project.
    """
    overview = RichText(
        title=_(u"Overview"),
        description=_(u"Description of data set for end user."),
        default_mime_type='text/structured',
        output_mime_type='text/html',
        allowed_mime_types=('text/structured','text/plain',),
        )

    queries = RichText(
        title=_(u"User Queries"),
        description=_(u"List of potential user queries."),
        required = False,
        )

    uri = schema.URI(
        title=_(u"Website"),
        description=_(u"Link to the data or other external source page"),
        required = False,
        )

    mechanism = schema.Choice(
        title=_(u"Query Mechanism"),
        description=_("Query mechanism used to access the data."),
        vocabulary=query_mechanism,
        required=False,
        default="undefined",
        )

    frequency = schema.Choice(
        title=_(u"Update Frequency"),
        description=_(u"How often is the data updated?"),
        vocabulary=update_frequency,
        default="undefined",
        )

    spatial_res = schema.Choice(
        title=_(u"Spatial Resolution"),
        description=_(u"Spatial scale on which the data is collected."),
        vocabulary=spatial_resolution,
        default="undefined",
        )

    temporal_res = schema.Choice(
        title=_(u"Temporal Resolution"),
        description=_(u"Time scale of the data (ie census is yearly)"),
        vocabulary=temporal_resolution,
        default="undefined",
        )

    considerations = RichText(
        title=_(u"Technical Considerations"),
        description=_(u"How data will be processed within CyberGIS"),
        default_mime_type='text/structured',
        output_mime_type='text/html',
        allowed_mime_types=('text/structured','text/plain',),
        required = False,
        )


# Custom content-type class; selfects created for this content type will
# be instances of this class. Use this class to add content-type specific
# methods and properties. Put methods that are mainly useful for rendering
# in separate view classes.

class Data(Container):

    # Add your class methods and properties here
    def to_dict(self):
        fields = {'overview':'', 'queries':'',
                'uri':'', 'mechanism':'', 'frequency': '',
                'spatial_res':'', 'temporal_res':'', 'considerations':''}
        if(self.overview):
            fields['overview'] = self.overview.raw
        if(self.queries):
            fields['queries'] = self.queries.raw
        if(self.uri):
            fields['uri'] = self.uri
        if(self.mechanism):
            fields['mechanism'] = self.mechanism
        if(self.frequency):
            fields['frequency'] = self.frequency
        if(self.spatial_res):
            fields['spatial_res'] = self.spatial_res
        if(self.temporal_res):
            fields['temporal_res'] = self.temporal_res
        if(self.considerations):
            fields['considerations'] = self.considerations.raw
        return fields


# View class
# The view is configured in configure.zcml. Edit there to change
# its public name. Unless changed, the view will be available
# TTW at content/@@dataview

class DataView(BrowserView):
    """ sample view class """
    index = ViewPageTemplateFile("data_templates/dataview.pt")

    def getUID(self) :
        context = self.aq_base
        uuid = IUUID(context,None)
        return uuid

    def render(self) :
        return self.index()
    # Add view methods here
    def dataTable(self):
        context = aq_inner(self.context)
        print context
        catalog = getToolByName(context, 'portal_catalog')
        print catalog

    def __init__(self,context,request) :
        self.context = context
        self.request = request

    def data(self) :
        context = aq_inner(self.context)
        catalog = getToolByName(context, 'portal_catalog')
        return (catalog(object_provides=IData.__identifier__,
                path='/'.join(context.getPhysicalPath()),
                sort_on='sortable_title'))
