from z3c.form import group, field
from zope import schema
from zope.interface import invariant, Invalid
from zope.schema.interfaces import IContextSourceBinder
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm

from plone.dexterity.content import Container

from plone.app.textfield import RichText
from plone.formwidget.namedfile import converter
from plone.namedfile.field import NamedImage, NamedFile
from plone.namedfile.field import NamedBlobImage, NamedBlobFile
from plone.namedfile.interfaces import IImageScaleTraversable

from z3c.relationfield.schema import RelationList, RelationChoice
from plone.formwidget.contenttree import ObjPathSourceBinder

from plone.supermodel import model
from Products.Five import BrowserView
from cybergis.poc import MessageFactory as _
from cybergis.poc.vocabulary import spatial_resolution, topic_query_mechanism
from Acquisition import aq_inner
from Products.CMFCore.utils import getToolByName

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.uuid.interfaces import IUUID
from plone.app.uuid.utils import uuidToObject

from werkzeug.urls import url_fix

import base64

from xml.etree import ElementTree as et

# Interface class; used to define content-type schema.
class ITopic(model.Schema, IImageScaleTraversable):
    """
    A topic of concern for the POC project.
    """
    overview = RichText(
            title=_(u"Overview"),
            output_mime_type="text/html",
            description=_(u"What is interesting about the topic? Why would "
                u"the topic be of useful to the members of the public? "
                ),
            required=True,
            )

    justification = RichText(
            title=_(u"Scientific Justification"),
            output_mime_type="text/html",
            description=_(u"Is there a scientific background for this topic? "
                u"Provide references and links to related material."
                ),
            required=False,
            )

    organization = schema.TextLine(
        title=_(u"Organization"),
        description=_(u"Organization responsible for the original data."),
        required = False,
        )

    attribution = schema.TextLine(
        title=_(u"Attribution"),
        description=_(u"Formal attribution shown when data is presented."),
        required = False,
        )

    image = NamedImage(
            title=_(u"Topic Image"),
            description=_(u"Image or icon representing the topic"),
            required=True
            )

    scale = schema.Choice(
            title=_(u"Regional Scale"),
            description=_(u""),
            vocabulary=spatial_resolution,
            required=True,
            default=None
            )

    q_mech = schema.Choice(
            title=_(u"Query Mechanism"),
            description=_(u"How will the data for the topic be queried?"),
            vocabulary=topic_query_mechanism,
            required=True,
            )

    q_url = schema.TextLine(
            title=_(u"Query URL"),
            description=_(u"URL to be used with the Query Mechanism"),
            required=True
            )

# Custom content-type class; objects created for this content type will
# in separate view classes.
class Topic(Container):

    # Add your class methods and properties here
    def to_dict(self):
        fields = {'overview':'', 'justification':'',
                    'organization':'', 'attribution':'',
                    'scale':'', 'q_mech':'',
                    'img_url':'', 'img_encoded':'',
                    'q_url':''}
        if (self.overview):
            fields['overview'] = self.overview.raw
        if (self.justification):
            fields['justification'] = self.justification.raw
        if (self.organization):
            fields['organization'] = self.organization
        if (self.attribution):
            fields['attribution'] = self.attribution
        if (self.scale):
            fields['scale'] = self.scale
        if (self.q_mech):
            fields['q_mech'] = self.q_mech
        if (self.image):
            img_url = 'http://141.142.168.38:8888'+self.absolute_url_path()+'/@@download/image/'+self.image.filename
            fields['img_url'] = url_fix(img_url)
            fields['img_encoded'] = self.encode_image()
        if (self.q_url and self.q_mech):
            if(self.q_mech == "Longitude/Latitude/Depth") :
                fields['q_url'] = self.q_url
        return fields

    def get_api_url(self):
        return "http://141.142.168.38:8888/Plone/@@API/topics/"+self.id

    def render_overview(self):
        if(self.overview) :
            return ''.join(et.fromstring(self.overview.output).itertext())

    def render_justification(self):
        if(self.justification) :
            return ''.join(et.fromstring(self.justification.output).itertext())

    def print_namedimage_as_dict(self):
        if(self.image) :
            return self.image.__dict__

    def encode_image(self):
        if(self.image) :
            return base64.b64encode(self.image._getData())
#           return converter.b64encode_file(self.image.filename,str(self.image._data))

# View class
# The view is configured in configure.zcml. Edit there to change
# its public name. Unless changed, the view will be available
# TTW at content/@@topicview
class TopicView(BrowserView):
    """ sample view class """
    index = ViewPageTemplateFile("topic_templates/topicview.pt")
    
    def getUID(self):
        context = self.aq_base
        uuid = IUUID(context, None)
        return uuid

    def render(self):
        return self.index()
    # Add view methods here
    def topicTable(self):
        context = aq_inner(self.context) 
        print context
        catalog = getToolByName(context, 'portal_catalog')
        print catalog

    def __init__(self, context, request):
        self.context = context
        self.request = request

    def get_context(self):
        return aq_inner(self.context)

    def topics(self):
        context = aq_inner(self.context)
        catalog = getToolByName(context, 'portal_catalog')
        print context.absolute_url_path()
        for topic in context.aq_inner.listFolderContents() :
            print topic
            print topic.render_overview()
            print topic.to_dict()['overview']
        return context.aq_inner.listFolderContents(
                    contentFilter={"portal_type":"cybergis.poc.topic"}
                )
