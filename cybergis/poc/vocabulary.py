from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm

from cybergis.poc import MessageFactory as _


spatial_resolution = SimpleVocabulary.fromItems((
    (_(u"Undefined"), "undefined"),
    (_(u"National"), "national"),
    (_(u"State"), "state"),
    (_(u"County"), "county"),
    (_(u"City"), "city"),
    (_(u"Neighborhood"), "neighborhood"),
    ))

temporal_resolution = SimpleVocabulary.fromItems((
    (_(u"Undefined"), "undefined"),
    (_(u"Yearly"), "yearly"),
    (_(u"Monthly"), "monthly"),
    (_(u"Weekly"), "weekly"),
    (_(u"Daily"), "daily"),
    (_(u"Hourly"), "hourly"),
    (_(u"Real Time"), "realtime"),
    ))

update_frequency = SimpleVocabulary.fromItems((
    (_(u"Undefined"), "undefined"),
    (_(u"Yearly"), "yearly"),
    (_(u"Monthly"), "monthly"),
    (_(u"Weekly"), "weekly"),
    (_(u"Daily"), "daily"),
    (_(u"Hourly"), "hourly"),
    (_(u"Real Time"), "realtime"),
    ))

query_mechanism = SimpleVocabulary.fromItems((
    (_(u"Undefined"), "undefined"),
    (_(u"Batch Download"), "download"),
    (_(u"Screen Scraping"), "scrape"),
    (_(u"REST API"), "rest"),
    (_(u"API (other)"), "api"),
    ))

topic_query_mechanism = SimpleVocabulary.fromItems((
    (_(u"Long/Lat/Depth"),"Longitude/Latitude/Depth"),
    ))
