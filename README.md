POC Analytics
=============

This is the central document to evolve the development of POC analytics 
in order to publish POC as a mobile app for the general public. The goal is 
to develop a set of big data analytical methods (at least one) that are 
scientifically sound, easy to understand by the general public, and represent 
big data analytics challenges. 

Strategies to develop such analytics include:

* Literature study of success stories in related areas - to capture the essence of developing such analytics

* Explore interesting datasets - data that has interesting geospatial knowledge to capture and present

* Prototype geospatial analysis methods for big data analytics.
